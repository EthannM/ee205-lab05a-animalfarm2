//////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file nene.cpp
/// @version 1.0
///
/// Exports data about all nene birds
///
/// @author @todo yourName <@todo yourMail@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   @todo dd_mmm_yyyy
///////////////////////////////////////////////////////////////////////////////
#include <string>
#include <iostream>

#include "nene.hpp"

using namespace std;

namespace animalfarm {

Nene::Nene( string tagId  , enum Color newColor, enum Gender newGender ) {
   gender = newGender;         /// Get from the constructor... not all nene  are the same gender (this is a has-a relationship)
   species = "Branta sandvicensis";    /// Hardcode this... all nen  are the same species (this is a is-a relationship)
   featherColor = newColor;       /// A has-a relationship, so it comes through the constructor
   isMigratory  = 1;       /// An is-a relationship, so it's safe to hardcode.
   tag  = tagId ;             /// A has-a relationship. What is its tag ID.
}


const string Nene::speak() {
   return string( "Nay, nay" );
}


/// Print our Nene  first... then print whatever information Bird holds.
void Nene::printInfo() {
   cout << "Nene" << endl;
   cout << "   Tag ID  = [" << tag  << "]" << endl;
   Bird::printInfo();
}
}


